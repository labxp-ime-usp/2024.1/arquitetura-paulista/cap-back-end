# Cartografia-da-Arquitetura-Paulistana

## Run Docker
NOTES:
1) Make sure docker, docker-compose and Java 17 are installed
2) Make sure docker deamons are running
* Make the file cartografia_paulistana.sh an executable file
```bash
chmod +x cartografia_paulistana.sh
```
* Then run the script to create the docker images and containers for this application.
It will create the docker containers for mysql (database) and the application
```bash
sudo ./cartografia_paulistana.sh
```

* Copy ```bash default.env``` as ```bash .env``` and change the information you inputted when running the script.

* Run ```bash docker-compose up```

* You can check if your database is working inside the container with
```bash
docker exec -it cartografia-paulistana_mysql_1 mysql -u root -p
```