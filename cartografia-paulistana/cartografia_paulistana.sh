#!/bin/bash
set -e
set -o pipefail

read -s -p "Enter the database root user password: " MYSQL_ROOT_PASSWORD
echo 

read -p "Enter the user name for cap batabase: " MYSQL_USERNAME

read -s -p "Enter the user password for cap batabase: " MYSQL_PASSWORD
echo 

export MYSQL_ROOT_PASSWORD
export MYSQL_USERNAME
export MYSQL_PASSWORD

# Check if docker is installed
if ! command -v docker &>/dev/null; then
    echo "Error: 'docker' is not installed. Aborting."
    exit 1
fi

# Check if docker-compose is installed
if ! command -v docker-compose &>/dev/null; then
    echo "Error: 'docker-compose' is not installed. Aborting."
    exit 1
fi

# Check if Java 17 is installed
if ! java -version 2>&1 | grep version | grep -q "17"; then
    echo "Java 17 is not installed."
    exit 1
fi

# Run docker-compose to bring up necessary services, clean and package the project using Maven,
# build a Docker image for the application, and finally run a container
# docker-compose up -d
if [[ $(docker-compose up -d 2>&1) == *"Is the docker daemon running?"* ]]; then
    echo "Error: Docker daemon is not running."
    exit 1;
fi

./mvnw clean package -DskipTests

docker build -t cartografia-paulistana .

docker run -e MYSQL_HOST='mysql' -e MYSQL_USERNAME="$MYSQL_USERNAME" -e MYSQL_PASSWORD="$MYSQL_PASSWORD" --network cartografia-paulistana_backend-network --link cartografia-paulistana-mysql-1 --name cartografia-paulistana cartografia-paulistana:latest
