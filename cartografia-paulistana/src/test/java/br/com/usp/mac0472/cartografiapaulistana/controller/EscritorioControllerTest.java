package br.com.usp.mac0472.cartografiapaulistana.controller;

import br.com.usp.mac0472.cartografiapaulistana.dto.escritorio.EscritorioUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.helper.builder.ArquitetoBuilder;
import br.com.usp.mac0472.cartografiapaulistana.helper.builder.EscritorioBuilder;
import br.com.usp.mac0472.cartografiapaulistana.helper.builder.EscritorioUpdateDtoBuilder;
import br.com.usp.mac0472.cartografiapaulistana.model.Escritorio;
import br.com.usp.mac0472.cartografiapaulistana.service.EscritorioService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class EscritorioControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private EscritorioService service;
    private EscritorioBuilder escritorioBuilder = null;
    private String json = null;
    private Escritorio escritorio = null;
    @BeforeEach
    public void setup() throws Exception {
        escritorioBuilder = new EscritorioBuilder();
        json = escritorioBuilder.jsonBuild();
        escritorio = escritorioBuilder.build();
        assertNotNull(json, "Json vazio!");
        assertNotNull(escritorio, "Escritorio vazio!");
    }

    @Test
    public void givenEscritorio_whenCreateEscritorio_thenReturnEscritorio() throws Exception {
        // Given
        when(service.createEscritorio(any(Escritorio.class)))
                .thenReturn(escritorio);

        // Then
        mvc.perform(post("/api/escritorios")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome", is(escritorio.getNome())));
    }

    @Test
    public void givenEscritorioCadastrado_whenGetEscritorios_thenReturnEscritorios() throws Exception {

        // Given
        PageRequest paginacao = PageRequest.of(0, 1);
        List<Escritorio> list_arq = new ArrayList<>(Arrays.asList(escritorio));
        Page<Escritorio> result = new PageImpl<Escritorio>(list_arq, paginacao, list_arq.size());

        assertNotNull(escritorio, "Escritorio vazio!");

        when(service.readEscritorios(any()))
                .thenReturn(result);

        // Then
        mvc.perform(get("/api/escritorios"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", is(escritorio.getId())));
    }

    @Test
    public void givenEscritorioCadastrado_whenGetEscritorioId_thenReturnFound() throws Exception {

        // Given
        Optional<Escritorio> result = Optional.of(escritorio);

        when(service.readEscritorio(any(Integer.class)))
                .thenReturn(result);

        // Then
        mvc.perform(get("/api/escritorios/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(escritorio.getId())));
    }

    @Test
    public void givenEscritorioNaoCadastrado_whenGetEscritorioId_thenReturnNotFound() throws Exception {

        // Given
        Optional<Escritorio> result = Optional.empty();

        when(service.readEscritorio(any()))
                .thenReturn(result);

        // Then
        mvc.perform(get("/api/escritorios/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenEscritorioId_whenUpdateEscritorio_thenReturnEscritorio() throws Exception {

        // Given
        EscritorioUpdateDtoBuilder dtoBuilder = new EscritorioUpdateDtoBuilder();
        String json = dtoBuilder.jsonBuild();
        assertNotNull(json, "Json vazio!");
        assertNotNull(escritorio, "Escritorio vazio!");
        Optional<Escritorio> result = Optional.of(escritorio);

        when(service.updateEscritorio(any(Integer.class), any(EscritorioUpdateDto.class)))
                .thenReturn(result);

        // Then
        mvc.perform(put("/api/escritorios/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is(escritorio.getNome())))
                .andExpect(jsonPath("$.id", is(escritorio.getId())));
    }

    @Test
    public void givenEscritorioId_whenDeleteEscritorio_returnEmptyBody() throws Exception
    {
        Random random = new Random();
        Integer id = random.nextInt(100);
        doNothing().when(service).deleteEscritorio(any(Integer.class));
        mvc.perform(delete("/api/escritorios/{id}", id))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenEscritorioId_whenMergeEscritorio_returnEscritorio() throws Exception
    {
        Random random = new Random();
        Integer id = random.nextInt(100);
        Optional<Escritorio> result = Optional.of(escritorio);
        EscritorioUpdateDtoBuilder dtoBuilder = new EscritorioUpdateDtoBuilder();
        String json = dtoBuilder.jsonBuild();
        when(service.mergeEscritorios(any(Integer.class))).thenReturn(result);
        mvc.perform(put("/api/escritorios/merge/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk());
    }

    @Test
    public void givenEscritorioId_Name_whenVerifyDuplicate_returnBool() throws Exception
    {
        Random random = new Random();
        Boolean result = false;
        String name = "escritorio USP";
        Integer id = random.nextInt(100);
        when(service.verifyNameDuplication(any(Integer.class), any(String.class))).thenReturn(result);
        mvc.perform(get("/api/escritorios/verify-duplicate/{id}", id)
                        .param("nome", name))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("false")));
    }
}
