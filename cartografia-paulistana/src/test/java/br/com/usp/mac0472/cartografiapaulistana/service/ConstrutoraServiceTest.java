package br.com.usp.mac0472.cartografiapaulistana.service;

import br.com.usp.mac0472.cartografiapaulistana.model.Construtora;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.repository.ConstrutoraRepository;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ConstrutoraServiceTest {
    @MockBean
    private ConstrutoraRepository repository;
    @MockBean
    private ObraRepository obraRepository;

    @Autowired
    private ConstrutoraService service;

    @Test
    @org.springframework.transaction.annotation.Transactional
    public void givenValidId_whenReadConstrutora_thenReturnConstrutora() {
        Construtora construtora = new Construtora();
        construtora.setId(1);
        when(repository.findById(1)).thenReturn(Optional.of(construtora));

        Optional<Construtora> result = service.readConstrutora(1);

        assertTrue(result.isPresent());
        assertEquals(1, result.get().getId());
    }
    @Test
    @Transactional
    public void givenIdAndName_whenVerifyDuplicate_thenReturnBool(){
        // Create test data
        Obra obra = new Obra();
        Construtora construtora = new Construtora(1, "Construtora USP", new HashSet<Obra>(List.of(obra)));
        Construtora construtoraDuplicado = new Construtora(2, "Construtora USP", new HashSet<Obra>(List.of(obra)));
        List<Construtora> construtoras = new ArrayList<Construtora>();

        when(repository.findAll()).thenReturn(construtoras);

        Boolean result = service.verifyNameDuplication(construtora.getId(), construtora.getNome());

        if(result) {
            assertEquals(construtora.getNome(), construtoraDuplicado.getNome());
            assertNotEquals(construtora.getId(), construtoraDuplicado.getId());
        }

        assertNotNull(result);
    }
    @Test
    @Transactional
    public void givenId_whenMerge_thenReturnMergedConstrutora(){
        Construtora construtora = new Construtora(1, "Construtora USP", new HashSet<Obra>());
        Construtora construtoraDuplicado = new Construtora(2, "Construtora USP", new HashSet<Obra>());
        List<Obra> obras = new ArrayList<Obra>();

        // Verify name duplication
        List<Construtora> construtoras = new ArrayList<Construtora>();
        construtoras.add(construtora);
        construtoras.add(construtoraDuplicado);
        when(repository.findAll()).thenReturn(construtoras);

        Boolean duplicated = service.verifyNameDuplication(construtora.getId(), construtora.getNome());
        assertNotNull(duplicated);
        assertTrue(duplicated);

        //Faz o merge dos duplicados se houver
        if(duplicated)
        {
            when(obraRepository.findObrasWhereConstrutoraId(construtora.getId())).thenReturn(obras);
            when(repository.getReferenceById(construtora.getId())).thenReturn((construtora));
            when(repository.getReferenceById(construtoraDuplicado.getId())).thenReturn((construtoraDuplicado));
            doNothing().when(repository).deleteById(construtora.getId());
            Optional<Construtora> result = service.mergeConstrutoras(construtora.getId());

            // Verify the merge result
            assertTrue(result.isPresent());
            assertEquals(construtoraDuplicado.getId(), result.get().getId());
        }
    }
}
