package br.com.usp.mac0472.cartografiapaulistana.helper.builder;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoCreateDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.enums.EnderecoTipo;
import br.com.usp.mac0472.cartografiapaulistana.enums.EnderecoTitulo;
import br.com.usp.mac0472.cartografiapaulistana.enums.ObraStatus;
import br.com.usp.mac0472.cartografiapaulistana.model.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ObraUpdateDtoBuilder {
    List<String> autoria = Arrays.asList("1", "2");
    public ObraUpdateDto build() {
        return new ObraUpdateDto("15.124141",
                "6.12464",
                "Torre Eiffel",
                autoria,
                2122,
                3333,
                4444,
                5555,
                6666,
                "Residencial",
                "7777",
                "Comercial",
                "8888",
                "CONSTRUIDO",
                autoria,
                null,
                1,
                2022,
                2023,
                2024,
                2025,
                1,
                List.of("google.com"),
                new EnderecoCreateDto(EnderecoTipo.ESPLANADA,
                        EnderecoTitulo.EDUCADOR,
                        "5 de Novembro",
                        5,
                        null,
                        "12345-123",
                        "Osasco"
                )
        );
    }

    public String jsonBuild() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"latitude\": \"15.124141\", \"longitude\": \"6.12464\", \"nomeOficial\": \"Torre Eiffel\", \"autoria\": [\"1\"], \"anoProjeto\": 2122, \"anoConstrucao\": 3333, \"condephaat\": 4444, \"conpresp\": 5555, \"iphan\": 6666, \"usoOriginal\": \"Residencial\", \"codigoOriginal\": \"7777\", \"usoAtual\": \"Comercial\", \"codigoAtual\": \"8888\", \"status\": \"CONSTRUIDO\", \"escritorio\": [\"1\"], \"nomeAlternativo\": null, \"construtora\": 1, \"dataUsoAtual\": 2022, \"anoDemolicao\": 2023, \"anoRestauro\": 2024, \"anoReforma\": 2025, \"arquitetoReforma\": 1, \"referencias\": [\"google.com\"]," +
                "\"endereco\": {\"tipoEndereco\": \"ESPLANADA\", \"tituloEndereco\": \"EDUCADOR\", \"logradouro\": \"5 de Novembro\", \"numero\": 5, \"complemento\": null, \"cep\": \"12345-123\", \"municipio\": \"Osasco\"}, \"validadoProfessora\": false,\"validadoDPH\": false, \"id\": 1}");
        return jsonObject.toString();
    }
}
