package br.com.usp.mac0472.cartografiapaulistana.helper.builder;

import java.util.Arrays;
import java.util.HashSet;

import br.com.usp.mac0472.cartografiapaulistana.model.*;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.usp.mac0472.cartografiapaulistana.enums.EnderecoTipo;
import br.com.usp.mac0472.cartografiapaulistana.enums.ObraStatus;

public class ObraBuilder {

    public Obra build() {
        return new Obra(1,
            "-23,548006",
            "-46,634035",
            "Edifício Azevedo Villares",
            null,
            1945,
            null,
            2007,
            null,
            "EDIFÍCIOS DE ESCRITÓRIOS",
            "CH731.18.4.11",
            "EDIFÍCIOS DE ESCRITÓRIOS",
            "CH731.18.4.11",
            ObraStatus.CONSTRUIDO,
            null,
            null,
            null,
            null,
            null,
            false,
            false,
            new HashSet<Referencia>(Arrays.asList( new Referencia("https://docs.google.com/document/d/11BsyAI5XzM_TGx85E4yslyMCoFOq4wxPNezt_N1TmoI/edit?usp=sharing"))),
            new Endereco(1,
                "15 de Novembro",
                89,
                null,
                "01016-040",
                "São Paulo",
                EnderecoTipo.RUA,
            null,
                null
            ),
            new Construtora(1,
                "Severo, Villares & Cia. Ltda.",
                null
            ),
            new Arquiteto(2,
                    "João",
                    "Dumont",
                    "Serra",
                    null),
            new HashSet<Arquiteto>(Arrays.asList( new Arquiteto(1,
            "Arnaldo",
            "Dumont",
            "Villares",
            null))),
            new HashSet<Escritorio>(Arrays.asList( new Escritorio(1,
                    "USP",
                    null)))
        );
    }

    public String jsonBuild() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"id\": 1,\"autoria\": [\"1\"],\"escritorio\": [\"1\"],\"nomeOficial\": \"Edifício Azevedo Villares\",\"nomeAlternativo\": null,\"endereco\": {\"tipoEndereco\": \"RUA\",\"enderecoTitulo\": null,\"logradouro\": \"15 de Novembro\",\"numero\": \"89\",\"complemento\": null,\"" +
                "\": \"01016-040\",\"municipio\": \"São Paulo\"},\"anoProjeto\": null,\"anoConstrucao\": \"1945\",\"construtora\": \"1\",\"condephaat\": \"\",\"conpresp\": \"2007\",\"iphan\": null,\"usoOriginal\": \"EDIFÍCIOS DE ESCRITÓRIOS\",\"codigoOriginal\": \"CH731.18.4.11\",\"usoAtual\": \"EDIFÍCIOS DE ESCRITÓRIOS\",\"codigoAtual\": \"CH731.18.4.11\",\"dataUsoAtual\": null,\"status\": \"CONSTRUIDO\",\"anoDemolicao\": null,\"anoRestauro\": null,\"anoReforma\": null,\"arquitetoReforma\": 2,\"latitude\": \"-23,548006\",\"longitude\": \"-46,634035\",\"referencias\": [\"https://docs.google.com/document/d/11BsyAI5XzM_TGx85E4yslyMCoFOq4wxPNezt_N1TmoI/edit?usp=sharing\"],\"validadoProfessora\": false,\"validadoDPH\": false}");
        return jsonObject.toString();
    }

    public Obra buildObraDadosEssenciais() {
        return new Obra(1,
                "-23,548006",
                "-46,634035",
                "Edifício Azevedo Villares",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false,
                false,
                null,
                new Endereco(1,
                        "15 de Novembro",
                        89,
                        null,
                        "01016-040",
                        "São Paulo",
                        EnderecoTipo.RUA,
                        null,
                        null
                ),
                null,
                null,
                null,
                null
        );
    }
}
