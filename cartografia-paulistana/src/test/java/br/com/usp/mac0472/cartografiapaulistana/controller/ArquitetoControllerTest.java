package br.com.usp.mac0472.cartografiapaulistana.controller;

import br.com.usp.mac0472.cartografiapaulistana.dto.arquiteto.ArquitetoUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.helper.builder.ArquitetoBuilder;
import br.com.usp.mac0472.cartografiapaulistana.helper.builder.ArquitetoUpdateDtoBuilder;
import br.com.usp.mac0472.cartografiapaulistana.model.Arquiteto;
import br.com.usp.mac0472.cartografiapaulistana.service.ArquitetoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ArquitetoControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ArquitetoService service;

    private ArquitetoBuilder arquitetoBuilder = null;
    private String json = null;
    private Arquiteto arquiteto = null;
    @BeforeEach
    public void setup() throws Exception {
        arquitetoBuilder = new ArquitetoBuilder();
        json = arquitetoBuilder.jsonBuild();
        arquiteto = arquitetoBuilder.build();
        assertNotNull(json, "Json vazio!");
        assertNotNull(arquiteto, "Arquiteto vazio!");
    }

    @Test
    public void givenArq_whenCreateArq_thenReturnArq() throws Exception {
        when(service.createArquiteto(any(Arquiteto.class)))
                .thenReturn(arquiteto);

        // Then
        mvc.perform(post("/api/arquitetos")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome", is(arquiteto.getNome())))
                .andExpect(jsonPath("$.sobrenome", is(arquiteto.getSobrenome())));
    }

    @Test
    public void givenArqCadastrado_whenGetArqs_thenReturnArqs() throws Exception {

        // Given
        PageRequest paginacao = PageRequest.of(0, 1);
        List<Arquiteto> list_arq = new ArrayList<>(Arrays.asList(arquiteto));
        Page<Arquiteto> result = new PageImpl<Arquiteto>(list_arq, paginacao, list_arq.size());

        assertNotNull(arquiteto, "Arquiteto vazio!");

        when(service.readArquitetos(any()))
                .thenReturn(result);

        // Then
        mvc.perform(get("/api/arquitetos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", is(arquiteto.getId())));
    }

    @Test
    public void givenArqCadastrado_whenGetArqId_thenReturnFound() throws Exception {

        // Given
        Optional<Arquiteto> result = Optional.of(arquiteto);

        when(service.readArquiteto(any(Integer.class)))
                .thenReturn(result);

        // Then
        mvc.perform(get("/api/arquitetos/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(arquiteto.getId())));
    }

    @Test
    public void givenArqNaoCadastrado_whenGetArqId_thenReturnNotFound() throws Exception {

        // Given
        Optional<Arquiteto> result = Optional.empty();

        when(service.readArquiteto(any()))
                .thenReturn(result);

        // Then
        mvc.perform(get("/api/arquitetos/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenArqId_whenUpdateArq_thenReturnArq() throws Exception {

        // Given
        ArquitetoUpdateDtoBuilder dtoBuilder = new ArquitetoUpdateDtoBuilder();
        String json = dtoBuilder.jsonBuild();
        assertNotNull(json, "Json vazio!");
        assertNotNull(arquiteto, "Arquiteto vazio!");
        Optional<Arquiteto> result = Optional.of(arquiteto);

        when(service.updateArquiteto(any(Integer.class), any(ArquitetoUpdateDto.class)))
                .thenReturn(result);

        // Then
        mvc.perform(put("/api/arquitetos/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is(arquiteto.getNome())))
                .andExpect(jsonPath("$.sobrenome", is(arquiteto.getSobrenome())))
                        .andExpect(jsonPath("$.id", is(arquiteto.getId())));
    }

    @Test
    public void givenArqId_whenDeleteArq_returnEmptyBody() throws Exception
    {
        Random random = new Random();
        Integer id = random.nextInt(100);
        doNothing().when(service).deleteArquiteto(any(Integer.class));
        mvc.perform(delete("/api/arquitetos/{id}", id))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenArqId_whenMergeArq_returnArq() throws Exception
    {
        Random random = new Random();
        Integer id = random.nextInt(100);
        Optional<Arquiteto> result = Optional.of(arquiteto);
        ArquitetoUpdateDtoBuilder dtoBuilder = new ArquitetoUpdateDtoBuilder();
        String json = dtoBuilder.jsonBuild();
        when(service.mergeArquitetos(any(Integer.class))).thenReturn(result);
        mvc.perform(put("/api/arquitetos/merge/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk());
    }

    @Test
    public void givenArqId_Name_whenVerifyDuplicate_returnBool() throws Exception
    {
        Random random = new Random();
        Boolean result = false;
        String name = "João da Silva";
        Integer id = random.nextInt(100);
        when(service.verifyNameDuplication(any(Integer.class), any(String.class))).thenReturn(result);
        mvc.perform(get("/api/arquitetos/verify-duplicate/{id}", id)
                        .param("nome", name))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("false")));
    }
}
