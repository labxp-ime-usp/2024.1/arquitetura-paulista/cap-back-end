package br.com.usp.mac0472.cartografiapaulistana.helper.builder;

import br.com.usp.mac0472.cartografiapaulistana.enums.EnderecoTipo;
import br.com.usp.mac0472.cartografiapaulistana.enums.ObraStatus;
import br.com.usp.mac0472.cartografiapaulistana.model.*;
import br.com.usp.mac0472.cartografiapaulistana.service.ArquitetoService;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ArquitetoBuilder {
    public Arquiteto build() {
        return new Arquiteto(1,
                "Manoel",
                "da",
                "Silva",
                new HashSet<Obra>(List.of(new Obra(1,
                        "-23,548006",
                        "-46,634035",
                        "Edifício Azevedo Villares",
                        null,
                        1945,
                        null,
                        2007,
                        null,
                        "EDIFÍCIOS DE ESCRITÓRIOS",
                        "CH731.18.4.11",
                        "EDIFÍCIOS DE ESCRITÓRIOS",
                        "CH731.18.4.11",
                        ObraStatus.CONSTRUIDO,
                        null,
                        null,
                        null,
                        null,
                        null,
                        false,
                        false,
                        new HashSet<Referencia>(Arrays.asList(new Referencia("https://docs.google.com/document/d/11BsyAI5XzM_TGx85E4yslyMCoFOq4wxPNezt_N1TmoI/edit?usp=sharing"))),
                        new Endereco(1,
                                "15 de Novembro",
                                89,
                                null,
                                "01016-040",
                                "São Paulo",
                                EnderecoTipo.RUA,
                                null,
                                null
                        ),
                        new Construtora(1,
                                "Severo, Villares & Cia. Ltda.",
                                null
                        ),
                        new Arquiteto(2,
                                "João",
                                "Dumont",
                                "Serra",
                                null),
                        new HashSet<Arquiteto>(Arrays.asList(new Arquiteto(1,
                                "Arnaldo",
                                "Dumont",
                                "Villares",
                                null))),
                        new HashSet<Escritorio>(Arrays.asList(new Escritorio(1,
                                "USP",
                                null))))
                )
            )
        );
    }

    public Arquiteto buildSemMeio() {
        return new Arquiteto(1,
                "Manoel",
                null,
                "Silva",
                new HashSet<Obra>(List.of(new Obra(1,
                        "-23,548006",
                        "-46,634035",
                        "Edifício Azevedo Villares",
                        null,
                        1945,
                        null,
                        2007,
                        null,
                        "EDIFÍCIOS DE ESCRITÓRIOS",
                        "CH731.18.4.11",
                        "EDIFÍCIOS DE ESCRITÓRIOS",
                        "CH731.18.4.11",
                        ObraStatus.CONSTRUIDO,
                        null,
                        null,
                        null,
                        null,
                        null,
                        false,
                        false,
                        new HashSet<Referencia>(Arrays.asList(new Referencia("https://docs.google.com/document/d/11BsyAI5XzM_TGx85E4yslyMCoFOq4wxPNezt_N1TmoI/edit?usp=sharing"))),
                        new Endereco(1,
                                "15 de Novembro",
                                89,
                                null,
                                "01016-040",
                                "São Paulo",
                                EnderecoTipo.RUA,
                                null,
                                null
                        ),
                        new Construtora(1,
                                "Severo, Villares & Cia. Ltda.",
                                null
                        ),
                        new Arquiteto(2,
                                "João",
                                "Dumont",
                                "Serra",
                                null),
                        new HashSet<Arquiteto>(Arrays.asList(new Arquiteto(1,
                                "Arnaldo",
                                "Dumont",
                                "Villares",
                                null))),
                        new HashSet<Escritorio>(Arrays.asList(new Escritorio(1,
                                "USP",
                                null))))
                )
                )
        );
    }

    public String jsonBuild() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"id\": \"1\", \"nome\": \"Manoel\", \"nomeMeio\": \"da\", \"sobrenome\": \"Silva\", \"obra\": [\"1\"]}"
        );
        return jsonObject.toString();
    }
}
