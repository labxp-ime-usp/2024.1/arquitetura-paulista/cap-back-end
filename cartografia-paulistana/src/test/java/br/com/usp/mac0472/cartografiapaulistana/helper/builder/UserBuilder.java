package br.com.usp.mac0472.cartografiapaulistana.helper.builder;

import br.com.usp.mac0472.cartografiapaulistana.dto.usuario.UserCreateDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Usuario;
import org.json.JSONException;
import org.json.JSONObject;

public class UserBuilder {
    public Usuario build() {
        return new Usuario(1,
                "userLoginTeste",
                "userLoginTesteSenha",
                "numeroUSPTeste",
                "email@teste.com.br",
                "ADMIN"
        );
    }

    public Usuario buildLoggedUser() {
        return new Usuario(2,
                "loggedUser",
                "loggedUserSenha",
                "loggedUserNumeroUSP",
                "loggedUser@teste.com.br",
                "ADMIN"
        );
    }

    public String jsonBuild() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"login\": \"userLoginTeste\", \"senha\": \"userLoginTesteSenha\", \"numeroUsp\": \"numeroUSPTeste\", \"email\": \"email@teste.com.br\", \"autorizacao\": \"ADMIN\"}"
        );
        return jsonObject.toString();
    }
}
