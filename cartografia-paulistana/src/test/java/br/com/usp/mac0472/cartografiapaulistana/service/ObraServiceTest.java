package br.com.usp.mac0472.cartografiapaulistana.service;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.enums.EnderecoTipo;
import br.com.usp.mac0472.cartografiapaulistana.helper.builder.ObraBuilder;
import br.com.usp.mac0472.cartografiapaulistana.model.Endereco;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ObraServiceTest {
    @MockBean
    private ObraRepository repository;

    @Autowired
    private ObraService service;

    @Test
    @Transactional
    public void givenDadosEssenciais_whenCreateObra_thenRetornaObraSalva() {
        ObraBuilder builder = new ObraBuilder();
        Obra obraSalva = builder.buildObraDadosEssenciais();

        // Create test data
        Obra obra = new Obra();
        List<String> arquitetosId = new ArrayList<>();
        arquitetosId.add(null);
        List<String> escritoriosId = new ArrayList<>();
        escritoriosId.add(null);
        Endereco endereco = new Endereco(1, "15 de Novembro", 89, null, "01016-040", "São Paulo", EnderecoTipo.RUA, null, null);
        List<String> referenciasUrls = new ArrayList<>();
        referenciasUrls.add(null);

        when(repository.save(any(Obra.class))).thenReturn(obraSalva);

        Obra savedObra = service.createObra(obra, arquitetosId, null, null, escritoriosId, endereco, referenciasUrls);

        assertNotNull(savedObra);
        assertNotNull(savedObra.getId());
    }

    @Test
    @Transactional
    public void whenReadObras_thenReturnAllObras() {
        Pageable pageable = Pageable.unpaged();
        List<Obra> obras = new ArrayList<>();
        obras.add(new Obra());
        obras.add(new Obra());
        Page<Obra> obrasPage = new PageImpl<>(obras);

        when(repository.findObras(pageable)).thenReturn(obrasPage);

        Page<Obra> result = service.readObras(pageable);

        assertNotNull(result);
        assertEquals(2, result.getTotalElements());
    }

    @Test
    @Transactional
    public void givenValidId_whenReadObra_thenReturnObra() {
        Obra obra = new Obra();
        obra.setId(1);
        when(repository.findById(1)).thenReturn(Optional.of(obra));

        Optional<Obra> result = service.readObra(1);

        assertTrue(result.isPresent());
        assertEquals(1, result.get().getId());
    }

    @Test
    @Transactional
    public void givenInvalidId_whenReadObra_thenReturnEmpty() {
        when(repository.findById(1)).thenReturn(Optional.empty());

        Optional<Obra> result = service.readObra(1);

        assertFalse(result.isPresent());
    }

    @Test
    @Transactional
    public void givenValidId_whenDeleteObra_thenNoExceptionThrown() {
        doNothing().when(repository).deleteById(1);

        assertDoesNotThrow(() -> service.deleteObra(1));

        verify(repository, times(1)).deleteById(1);
    }

    @Test
    @Transactional
    public void givenValidId_whenUpdateObra_thenObraUpdated() {
        Obra existingObra = new Obra();
        existingObra.setId(1);
        Endereco endereco = new Endereco(1, "15 de Novembro", 89, null, "01016-040", "São Paulo", EnderecoTipo.RUA, null, null);
        ObraUpdateDto updatedObraDto = new ObraUpdateDto(
                null, null, null, new ArrayList<>(), null, null, null, null, null, 
                null, null, null, null, null, new ArrayList<>(), null, null, null, null, null,
                null, null, new ArrayList<>(), null
        );

        when(repository.getReferenceById(1)).thenReturn(existingObra);
        when(repository.save(any(Obra.class))).thenReturn(existingObra);

        Optional<Obra> result = service.updateObra(1, updatedObraDto, endereco);

        assertTrue(result.isPresent());
        assertEquals(1, result.get().getId());
    }

    @Test
    @Transactional
    public void givenValidId_whenValidacaoProfessora_thenObraValidated() {
        Obra obra = new Obra();
        obra.setId(1);
        obra.setValidadoProfessora(false);
        when(repository.findById(1)).thenReturn(Optional.of(obra));
        when(repository.save(any(Obra.class))).thenReturn(obra);

        Obra result = service.validacaoProfessora(1);

        assertNotNull(result);
        assertTrue(result.getValidadoProfessora());
    }
}
