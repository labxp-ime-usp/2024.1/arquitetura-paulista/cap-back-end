package br.com.usp.mac0472.cartografiapaulistana.helper.builder;

import br.com.usp.mac0472.cartografiapaulistana.dto.arquiteto.ArquitetoUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.enums.EnderecoTipo;
import br.com.usp.mac0472.cartografiapaulistana.enums.ObraStatus;
import br.com.usp.mac0472.cartografiapaulistana.model.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ArquitetoUpdateDtoBuilder {
    List<String> obras = Arrays.asList("1", "2");
    public ArquitetoUpdateDto build() {
        return new ArquitetoUpdateDto(
                "Manoel",
                "da",
                "Silva"
        );
    }

    public String jsonBuild() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"id\": \"1\", \"nome\": \"Manoel\", \"nomeMeio\": \"da\", \"sobrenome\": \"Silva\"}"
        );
        return jsonObject.toString();
    }
}
