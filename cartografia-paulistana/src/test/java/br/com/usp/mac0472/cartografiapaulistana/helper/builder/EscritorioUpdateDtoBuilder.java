package br.com.usp.mac0472.cartografiapaulistana.helper.builder;

import br.com.usp.mac0472.cartografiapaulistana.dto.escritorio.EscritorioUpdateDto;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class EscritorioUpdateDtoBuilder {
    List<String> obras = Arrays.asList("1", "2");
    public EscritorioUpdateDto build() {
        return new EscritorioUpdateDto(
                "Doof Inc."
        );
    }

    public String jsonBuild() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"id\": \"1\", \"nome\": \"Doof Inc.\"}"
        );
        return jsonObject.toString();
    }
}
