package br.com.usp.mac0472.cartografiapaulistana.controller;

import br.com.usp.mac0472.cartografiapaulistana.helper.builder.UserBuilder;
import br.com.usp.mac0472.cartografiapaulistana.model.Usuario;
import br.com.usp.mac0472.cartografiapaulistana.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class AutenticacaoControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    /* TESTES POST /api/auth/cadastro */

    @Test
    public void givenDadosUsuario_whenCriaUsuarioUnico_thenRetornaSucesso() throws Exception {
        // given
        UserBuilder userBuilder = new UserBuilder();
        String json = userBuilder.jsonBuild();
        Usuario usuario = userBuilder.build();

        // when
        when(userRepository.findByLogin(any())).thenReturn(null);
        when(userRepository.findByEmail(any())).thenReturn(null);
        when(userRepository.findByNumeroUsp(any())).thenReturn(null);
        when(userRepository.save(any())).thenReturn(usuario);

        // then
        mvc.perform(post("/api/auth/cadastro")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl("/api/usuarios/" + usuario.getId()));
    }

    @Test
    public void givenDadosUsuario_whenCriaUsuarioLoginExistente_thenRetornaErro() throws Exception {
        // given
        UserBuilder userBuilder = new UserBuilder();
        String json = userBuilder.jsonBuild();
        Usuario usuario = userBuilder.build();

        // when
        when(userRepository.findByLogin(any())).thenReturn(usuario);
        when(userRepository.findByEmail(any())).thenReturn(null);
        when(userRepository.findByNumeroUsp(any())).thenReturn(null);

        // then
        mvc.perform(post("/api/auth/cadastro")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenDadosUsuario_whenCriaUsuarioEmailExistente_thenRetornaErro() throws Exception {
        // given
        UserBuilder userBuilder = new UserBuilder();
        String json = userBuilder.jsonBuild();
        Usuario usuario = userBuilder.build();

        // when
        when(userRepository.findByLogin(any())).thenReturn(null);
        when(userRepository.findByEmail(any())).thenReturn(usuario);
        when(userRepository.findByNumeroUsp(any())).thenReturn(null);

        // then
        mvc.perform(post("/api/auth/cadastro")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenDadosUsuario_whenCriaUsuarioNumeroUspExistente_thenRetornaErro() throws Exception {
        // given
        UserBuilder userBuilder = new UserBuilder();
        String json = userBuilder.jsonBuild();
        Usuario usuario = userBuilder.build();

        // when
        when(userRepository.findByLogin(any())).thenReturn(null);
        when(userRepository.findByEmail(any())).thenReturn(null);
        when(userRepository.findByNumeroUsp(any())).thenReturn(usuario);

        // then
        mvc.perform(post("/api/auth/cadastro")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest());
    }
}
