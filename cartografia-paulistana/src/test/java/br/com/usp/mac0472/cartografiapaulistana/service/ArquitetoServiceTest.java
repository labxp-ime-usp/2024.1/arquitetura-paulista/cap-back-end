package br.com.usp.mac0472.cartografiapaulistana.service;

import br.com.usp.mac0472.cartografiapaulistana.model.Arquiteto;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.repository.ArquitetoRepository;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ArquitetoServiceTest {
    @MockBean
    private ArquitetoRepository repository;
    @MockBean
    private ObraRepository obraRepository;

    @Autowired
    private ArquitetoService service;

    @Test
    @org.springframework.transaction.annotation.Transactional
    public void givenValidId_whenReadArq_thenReturnArq() {
        Arquiteto arquiteto = new Arquiteto();
        arquiteto.setId(1);
        when(repository.findById(1)).thenReturn(Optional.of(arquiteto));

        Optional<Arquiteto> result = service.readArquiteto(1);

        assertTrue(result.isPresent());
        assertEquals(1, result.get().getId());
    }

    @Test
    @Transactional
    public void givenIdAndName_whenVerifyDuplicate_thenReturnBool(){
        // Create test data
        Obra obra = new Obra();
        Arquiteto arquiteto = new Arquiteto(1, "João", "da", "Silva", new HashSet<Obra>(List.of(obra)));
        Arquiteto arquitetoDuplicado = new Arquiteto(2, "João", "da", "Silva", new HashSet<Obra>(List.of(obra)));
        List<Arquiteto> arquitetos = new ArrayList<Arquiteto>();

        when(repository.findAll()).thenReturn(arquitetos);

        Boolean result = service.verifyNameDuplication(arquitetoDuplicado.getId(), arquiteto.getNomeCompleto());

        if(result) {
            assertEquals(arquiteto.getNomeCompleto(), arquitetoDuplicado.getNomeCompleto());
            assertNotEquals(arquiteto.getId(), arquitetoDuplicado.getId());
        }

        assertNotNull(result);
    }

    @Test
    @Transactional
    public void givenId_whenMerge_thenReturnMergedArq(){
        Arquiteto arquiteto = new Arquiteto(1, "João", "da", "Silva", new HashSet<Obra>());
        Arquiteto arquitetoDuplicado = new Arquiteto(2, "João", "da", "Silva", new HashSet<Obra>());
        List<Obra> obras = new ArrayList<Obra>();

        // Verify name duplication
        List<Arquiteto> arquitetos = new ArrayList<Arquiteto>();
        arquitetos.add(arquiteto);
        arquitetos.add(arquitetoDuplicado);
        when(repository.findAll()).thenReturn(arquitetos);

        Boolean duplicated = service.verifyNameDuplication(arquiteto.getId(), arquiteto.getNomeCompleto());
        assertNotNull(duplicated);
        assertTrue(duplicated);

        //Faz o merge dos duplicados se houver
        if(duplicated)
        {
            when(obraRepository.findObrasWhereEscritorioId(arquiteto.getId())).thenReturn(obras);
            when(repository.getReferenceById(arquiteto.getId())).thenReturn((arquiteto));
            when(repository.getReferenceById(arquitetoDuplicado.getId())).thenReturn((arquitetoDuplicado));
            doNothing().when(repository).deleteById(arquiteto.getId());
            Optional<Arquiteto> result = service.mergeArquitetos(arquiteto.getId());

            // Verify the merge result
            assertTrue(result.isPresent());
            assertEquals(arquitetoDuplicado.getId(), result.get().getId());
        }
    }
}
