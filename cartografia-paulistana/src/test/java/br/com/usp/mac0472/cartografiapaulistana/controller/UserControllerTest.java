package br.com.usp.mac0472.cartografiapaulistana.controller;

import br.com.usp.mac0472.cartografiapaulistana.helper.builder.UserBuilder;
import br.com.usp.mac0472.cartografiapaulistana.model.Usuario;
import br.com.usp.mac0472.cartografiapaulistana.repository.UserRepository;
import br.com.usp.mac0472.cartografiapaulistana.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserService userService;

    /* TESTES DELETE /api/usuarios/{id} */
    @Test
    public void givenIDUsuarioValido_whenIDNaoEUsuarioLogado_thenDeletaUsuario() throws Exception {
        // given
        UserBuilder userBuilder = new UserBuilder();
        Usuario usuarioParaDeletar = userBuilder.build();
        Usuario usuarioLogado = userBuilder.buildLoggedUser();
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);

        // when
        when(userRepository.findByLogin(any())).thenReturn(usuarioLogado);
        when(userService.readUsuario(any())).thenReturn(usuarioParaDeletar);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(usuarioLogado);
        SecurityContextHolder.setContext(securityContext);

        // then
        mvc.perform(delete("/api/usuarios/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenIDUsuarioValido_whenIdEhDOUsuarioLogado_thenNaoDeletaUsuario() throws Exception {
        // given
        UserBuilder userBuilder = new UserBuilder();
        Usuario usuarioParaDeletar = userBuilder.buildLoggedUser();
        Usuario usuarioLogado = userBuilder.buildLoggedUser();
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);

        // when
        when(userRepository.findByLogin(any())).thenReturn(usuarioLogado);
        when(userService.readUsuario(any())).thenReturn(usuarioParaDeletar);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(usuarioLogado);
        SecurityContextHolder.setContext(securityContext);

        // then
        mvc.perform(delete("/api/usuarios/2"))
                .andExpect(status().isBadRequest());
    }
}
