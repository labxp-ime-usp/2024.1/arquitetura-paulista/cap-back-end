package br.com.usp.mac0472.cartografiapaulistana.service;

import br.com.usp.mac0472.cartografiapaulistana.model.Escritorio;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.repository.EscritorioRepository;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EscritorioServiceTest {
    @MockBean
    private EscritorioRepository repository;
    @MockBean
    private ObraRepository obraRepository;

    @Autowired
    private EscritorioService service;

    @Test
    @org.springframework.transaction.annotation.Transactional
    public void givenValidId_whenReadEscritorio_thenReturnEsscritorio() {
        Escritorio escritorio = new Escritorio();
        escritorio.setId(1);
        when(repository.findById(1)).thenReturn(Optional.of(escritorio));

        Optional<Escritorio> result = service.readEscritorio(1);

        assertTrue(result.isPresent());
        assertEquals(1, result.get().getId());
    }
    @Test
    @Transactional
    public void givenIdAndName_whenVerifyDuplicate_thenReturnBool(){
        // Create test data
        Obra obra = new Obra();
        Escritorio escritorio = new Escritorio(1, "Escritorio USP", new HashSet<Obra>(List.of(obra)));
        Escritorio escritorioDuplicado = new Escritorio(2, "Escritorio USP", new HashSet<Obra>(List.of(obra)));
        List<Escritorio> escritorios = new ArrayList<Escritorio>();

        when(repository.findAll()).thenReturn(escritorios);

        Boolean result = service.verifyNameDuplication(escritorio.getId(), escritorio.getNome());

        if(result) {
            assertEquals(escritorio.getNome(), escritorioDuplicado.getNome());
            assertNotEquals(escritorio.getId(), escritorioDuplicado.getId());
        }

        assertNotNull(result);
    }
    @Test
    @Transactional
    public void givenId_whenMerge_thenReturnMergedEscritorio(){
        Escritorio escritorio = new Escritorio(1, "Escritorio USP", new HashSet<Obra>());
        Escritorio escritorioDuplicado = new Escritorio(2, "Escritorio USP", new HashSet<Obra>());
        List<Obra> obras = new ArrayList<Obra>();

        // Verify name duplication
        List<Escritorio> escritorios = new ArrayList<Escritorio>();
        escritorios.add(escritorio);
        escritorios.add(escritorioDuplicado);
        when(repository.findAll()).thenReturn(escritorios);

        Boolean duplicated = service.verifyNameDuplication(escritorio.getId(), escritorio.getNome());
        assertNotNull(duplicated);
        assertTrue(duplicated);

        //Faz o merge dos duplicados se houver
        if(duplicated)
        {
            when(obraRepository.findObrasWhereEscritorioId(escritorio.getId())).thenReturn(obras);
            when(repository.getReferenceById(escritorio.getId())).thenReturn((escritorio));
            when(repository.getReferenceById(escritorioDuplicado.getId())).thenReturn((escritorioDuplicado));
            doNothing().when(repository).deleteById(escritorio.getId());
            Optional<Escritorio> result = service.mergeEscritorios(escritorio.getId());

            // Verify the merge result
            assertTrue(result.isPresent());
            assertEquals(escritorioDuplicado.getId(), result.get().getId());
        }
    }

}
