package br.com.usp.mac0472.cartografiapaulistana.service;

import java.util.*;

import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.usp.mac0472.cartografiapaulistana.dto.arquiteto.ArquitetoUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Arquiteto;
import br.com.usp.mac0472.cartografiapaulistana.repository.ArquitetoRepository;
import jakarta.transaction.Transactional;

import static br.com.usp.mac0472.cartografiapaulistana.utils.NameDuplicationUtil.normalizeString;

@Service
public class ArquitetoService {

	@Autowired
	private ArquitetoRepository repository;
	@Autowired
	private ObraRepository obraRepository;
	private Integer duplicatedArqId;
	public Page<Arquiteto> readArquitetos(Pageable pageable) {
		return repository.findAll(Pageable.unpaged());
	}

	public Optional<Arquiteto> readArquiteto(Integer id) {
		return repository.findById(id);
	}

	@Transactional
	public Arquiteto createArquiteto(Arquiteto arquiteto) {
		return repository.save(arquiteto);
	}

	@Transactional
	public Optional<Arquiteto> updateArquiteto(Integer id, ArquitetoUpdateDto updatedArquiteto) {
		Arquiteto existingArquiteto = repository.getReferenceById(id);
		existingArquiteto.update(updatedArquiteto);
		repository.save(existingArquiteto);
		return Optional.ofNullable(existingArquiteto);
	}

	public Boolean verifyNameDuplication(Integer id, String nome)
	{
		List<Arquiteto> arquitetos = repository.findAll();
		for (Arquiteto arquiteto : arquitetos) {
			String nomeExistente = normalizeString(arquiteto.getNomeCompleto());
			if (nomeExistente.equals(normalizeString(nome)) && !Objects.equals(arquiteto.getId(), id)) {
				duplicatedArqId = arquiteto.getId();
				return true;
			}
		}
		return false;
	}

	@Transactional
	public Optional<Arquiteto> mergeArquitetos(Integer id)
	{
		List<Obra> obras = obraRepository.findObrasWhereArqId(id);
		Arquiteto editedArquiteto = repository.getReferenceById(id);
		Arquiteto existingArquiteto = repository.getReferenceById(duplicatedArqId);
		if(obras != null)
		{
			for(Obra obra : obras){
				Set<Arquiteto> arquitetosObra = new HashSet<>(obra.getArquitetos().stream().toList());
				arquitetosObra.add(existingArquiteto);
				arquitetosObra.remove(editedArquiteto);
				obra.getArquitetos().clear();
				obra.getArquitetos().addAll(arquitetosObra);
			}
		}
		List<Obra> obrasReforma = obraRepository.findObrasWhereArqReformaId(id);
		if(obrasReforma != null)
		{
			for(Obra obra : obrasReforma){
				obra.setArquitetoReforma(existingArquiteto);
			}
		}
		duplicatedArqId = null;
		this.repository.deleteById(id);
		return Optional.of(existingArquiteto);
	}

	@Transactional
	public void deleteArquiteto(Integer id) {
		this.repository.deleteById(id);
	}

	public Page<Arquiteto> findArquitetos(Pageable pageable, String pesquisa) {
		return repository.findArquitetos(Pageable.unpaged(), pesquisa);
	}

}
