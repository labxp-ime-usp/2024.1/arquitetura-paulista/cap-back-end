package br.com.usp.mac0472.cartografiapaulistana.dto.escritorio;

import jakarta.validation.constraints.NotBlank;

public record EscritorioCreateDto(@NotBlank String nome){

}
