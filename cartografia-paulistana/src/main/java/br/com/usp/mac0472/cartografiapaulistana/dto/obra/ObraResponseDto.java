package br.com.usp.mac0472.cartografiapaulistana.dto.obra;

import java.util.List;

import lombok.Data;

@Data
public class ObraResponseDto {

	private Integer id;
	private String latitude;
	private String longitude;
	private String nomeOficial;
	private List<Integer> autoria;
	private Integer anoProjeto;
	private Integer anoConstrucao;
	private Integer condephaat;
	private Integer conpresp;
	private Integer iphan;
	private String usoOriginal;
	private String codigoOriginal;
	private String usoAtual;
	private String codigoAtual;
	private String status;
	private List<Integer> escritorio;
	private String nomeAlternativo;
	private String construtora;
	private Integer dataUsoAtual;
	private Integer anoDemolicao;
	private Integer anoRestauro;
	private Integer anoReforma;
	private String arquitetoReforma;
	private List<String> referencias;
	private EnderecoResponseDto endereco;
	private Boolean validadoProfessora;
	private Boolean validadoDPH;

}
