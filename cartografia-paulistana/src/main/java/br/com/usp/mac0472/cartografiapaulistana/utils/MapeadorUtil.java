package br.com.usp.mac0472.cartografiapaulistana.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.TipoUsoResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.enums.UsoObra;
import br.com.usp.mac0472.cartografiapaulistana.model.Arquiteto;
import br.com.usp.mac0472.cartografiapaulistana.model.Escritorio;
import br.com.usp.mac0472.cartografiapaulistana.model.Referencia;
import org.modelmapper.ModelMapper;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraPageResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Endereco;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import io.micrometer.common.util.StringUtils;

import static br.com.usp.mac0472.cartografiapaulistana.utils.FormListsValidators.hasOnlyNullValues;

public abstract class MapeadorUtil {

	public static ObraPageResponseDto mapObraToPageResponse(Obra obra, ModelMapper mapper) {
		ObraPageResponseDto obraResponse = mapper.map(obra, ObraPageResponseDto.class);
		List<Integer> idsArquitetos = getIdsArquitetos(obra);
		obraResponse.setAnoProjeto(obra.getAnoProjeto());
		obraResponse.setEndereco(getEnderecoResponse(obra));
		obraResponse.setAutoria(idsArquitetos);
		return obraResponse;
	}
	
	public static ObraResponseDto mapObraToObraResponse(Obra obra, ModelMapper mapper) {
		ObraResponseDto obraResponse = mapper.map(obra, ObraResponseDto.class);
		List<Integer> idsArquitetos = getIdsArquitetos(obra);
		List<Integer> idsEscritorios = getIdsEscritorios(obra);
		if(obra.getConstrutora() != null){
			String construtoraId = obra.getConstrutora().getId().toString();
			obraResponse.setConstrutora(construtoraId);
		}
		if (!obra.getReferencias().isEmpty() && !hasOnlyNullValues(obra.getReferencias())) {
			List<String> referencias = obra.getReferencias().stream().map(Referencia::getUrl).toList();
			obraResponse.setReferencias(referencias);
		}
		EnderecoResponseDto enderecoResponse = getEnderecoResponse(obra);
		obraResponse.setAutoria(idsArquitetos);
		obraResponse.setEndereco(enderecoResponse);
		obraResponse.setEscritorio(idsEscritorios);
		if(obra.getArquitetoReforma() != null)
			obraResponse.setArquitetoReforma(obra.getArquitetoReforma().getId().toString());
		return obraResponse;
	}

	public static List<TipoUsoResponseDto> mapTiposUsoResponse()
	{
		return Arrays.stream(UsoObra.values()).map(usoObra -> new TipoUsoResponseDto(usoObra.getValue(), usoObra.getLabel()))
			.collect(Collectors.toList());
	}

	private static List<Integer> getIdsArquitetos(Obra obra)
	{
		List<Integer> idsArquitetos = obra.getArquitetos().stream()
				.map(Arquiteto::getId)
				.toList();
		return idsArquitetos;
	}
	private static List<Integer> getIdsEscritorios(Obra obra)
	{
		List<Integer> idsEscritorios = obra.getEscritorios().stream()
				.map(Escritorio::getId)
				.toList();
		return idsEscritorios;
	}
	private static List<String> getNomesArquitetos(Obra obra){
		List<String> nomesArquitetos = obra.getArquitetos().stream()
				.map(arquiteto -> 
				new StringBuilder()
				.append(arquiteto.getNome())
				.append(StringUtils.isNotBlank(arquiteto.getNomeMeio()) ? " " + arquiteto.getNomeMeio() + " " : " ")
				.append(arquiteto.getSobrenome())
				.toString())
				.toList();
		return nomesArquitetos;
	}
	
	private static EnderecoResponseDto getEnderecoResponse(Obra obra){
		Endereco endereco = obra.getEndereco();
		var enderecoResponse = new EnderecoResponseDto();
		enderecoResponse.setTipoEndereco(endereco.getTipoEndereco());
		enderecoResponse.setEnderecoTitulo(endereco.getEnderecoTitulo());
		enderecoResponse.setLogradouro(endereco.getLogradouro());
		enderecoResponse.setNumero(endereco.getNumero());
		enderecoResponse.setComplemento(endereco.getComplemento());
		enderecoResponse.setCep(endereco.getCep());
		enderecoResponse.setMunicipio(endereco.getMunicipio());
		return enderecoResponse;
	}
	
}
