package br.com.usp.mac0472.cartografiapaulistana.dto.obra;

import lombok.Data;

@Data
public class TipoUsoResponseDto {
    String label;
    String value;

    public TipoUsoResponseDto(String value, String label) {
        this.value = value;
        this.label = label;
    }
}
