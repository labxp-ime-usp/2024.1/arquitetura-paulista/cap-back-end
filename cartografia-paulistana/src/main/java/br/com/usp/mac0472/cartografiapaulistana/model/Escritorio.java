package br.com.usp.mac0472.cartografiapaulistana.model;

import br.com.usp.mac0472.cartografiapaulistana.dto.escritorio.EscritorioUpdateDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "escritorio")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Escritorio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nome")
    private String nome;

    @ManyToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
    @JoinTable(name = "escritorio_obra", joinColumns = { @JoinColumn(name = "escritorio_id") }, inverseJoinColumns = {
            @JoinColumn(name = "obra_id") })
    private Set<Obra> obras = new HashSet<>();

    public void update(EscritorioUpdateDto updatedEscritorio)
    {
        if(nonNull(updatedEscritorio.nome())){
            this.nome = updatedEscritorio.nome();
        }
    }
}
