package br.com.usp.mac0472.cartografiapaulistana.controller;

import br.com.usp.mac0472.cartografiapaulistana.dto.escritorio.EscritorioCreateDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.escritorio.EscritorioResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.escritorio.EscritorioUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Escritorio;
import br.com.usp.mac0472.cartografiapaulistana.service.EscritorioService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/escritorios")
@CrossOrigin(origins = "*")
public class EscritorioController {

	@Autowired
	private EscritorioService service;

	@Autowired
	private ModelMapper mapper;

	@GetMapping
	public ResponseEntity<Page<EscritorioResponseDto>> getEscritorios(Pageable pageable) {
		Page<Escritorio> escritorios = service.readEscritorios(pageable);
		List<EscritorioResponseDto> escritoriosDto = escritorios.stream()
				.map(escritorio -> mapper.map(escritorio, EscritorioResponseDto.class)).toList();
		Page<EscritorioResponseDto> response = PageableExecutionUtils.getPage(escritoriosDto, pageable,
				() -> escritorios.getTotalElements());
		return ResponseEntity.ok(response);
	}

	@GetMapping("/{id}")
	public ResponseEntity<EscritorioResponseDto> getEscritorio(@PathVariable Integer id) {
		Optional<Escritorio> escritorio = service.readEscritorio(id);
		if (escritorio.isPresent()) {
			EscritorioResponseDto response = mapper.map(escritorio.get(), EscritorioResponseDto.class);
			return ResponseEntity.ok(response);
		}
		return ResponseEntity.notFound().build();
	}

	@PostMapping
	public ResponseEntity<EscritorioResponseDto> createEscritorio(@RequestBody @Valid EscritorioCreateDto escritorioDto) {
		Escritorio escritorio = mapper.map(escritorioDto, Escritorio.class);
		service.createEscritorio(escritorio);
		EscritorioResponseDto response = mapper.map(escritorio, EscritorioResponseDto.class);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@PutMapping("/{id}")
	public ResponseEntity<EscritorioResponseDto> updateEscritorio(@PathVariable Integer id,
			@RequestBody EscritorioUpdateDto escritorioDto) {
		Optional<Escritorio> updatedEscritorio = service.updateEscritorio(id, escritorioDto);
		if (updatedEscritorio.isPresent()) {
			EscritorioResponseDto response = mapper.map(updatedEscritorio.get(), EscritorioResponseDto.class);
			return ResponseEntity.ok(response);
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping("/merge/{id}")
	public ResponseEntity<EscritorioResponseDto> mergeEscritorios(@PathVariable Integer id,
			@RequestBody EscritorioUpdateDto escritorioDto) {
		Optional<Escritorio> updatedEscritorio = service.mergeEscritorios(id);
		if (updatedEscritorio.isPresent()) {
			EscritorioResponseDto response = mapper.map(updatedEscritorio.get(), EscritorioResponseDto.class);
			return ResponseEntity.ok(response);
		}
		return ResponseEntity.notFound().build();
	}

	@GetMapping("/verify-duplicate/{id}")
	public ResponseEntity<Boolean> verifyNameDuplication(@PathVariable Integer id,
														 @RequestParam String nome) {
		return ResponseEntity.ok(service.verifyNameDuplication(id, nome));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteEscritorio(@PathVariable Integer id) {
		service.deleteEscritorio(id);
		return ResponseEntity.noContent().build();
	}

}
