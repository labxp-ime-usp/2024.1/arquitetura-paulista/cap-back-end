package br.com.usp.mac0472.cartografiapaulistana.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;

import br.com.usp.mac0472.cartografiapaulistana.model.Arquiteto;

@Repository
public interface ArquitetoRepository extends JpaRepository<Arquiteto, Integer> {

    @Query("SELECT o FROM Arquiteto o WHERE " +
		   "LOWER(CONCAT(o.nome, ' ', COALESCE(o.nomeMeio, ''), ' ', o.sobrenome)) LIKE LOWER(CONCAT('%',:pesquisa,'%'))"
	)
	Page<Arquiteto> findArquitetos(Pageable pageable, String pesquisa);

}
