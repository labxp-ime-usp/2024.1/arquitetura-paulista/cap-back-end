package br.com.usp.mac0472.cartografiapaulistana.dto.obra;

import java.util.List;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record ObraCreateDto(
		@NotNull String latitude, 
		@NotNull String longitude, 
		@NotBlank String nomeOficial,
		List<String> autoria,
		Integer anoProjeto,
		Integer anoConstrucao, 
		Integer condephaat, 
		Integer conpresp,
		Integer iphan,
		String usoOriginal,
		String codigoOriginal,
		String usoAtual,
		String codigoAtual,
		String status,
		List<String> escritorio,
		String nomeAlternativo,
		Integer construtora,
		Integer dataUsoAtual,
		Integer anoDemolicao,
		Integer anoRestauro,
		Integer anoReforma,
		Integer arquitetoReforma,
		List<String> referencias,
		@NotNull EnderecoCreateDto endereco
		) {

}
