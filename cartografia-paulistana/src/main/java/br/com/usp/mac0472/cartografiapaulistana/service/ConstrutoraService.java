package br.com.usp.mac0472.cartografiapaulistana.service;

import java.util.*;

import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.usp.mac0472.cartografiapaulistana.dto.construtora.ConstrutoraUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Construtora;
import br.com.usp.mac0472.cartografiapaulistana.repository.ConstrutoraRepository;
import jakarta.transaction.Transactional;

import static br.com.usp.mac0472.cartografiapaulistana.utils.NameDuplicationUtil.normalizeString;

@Service
public class ConstrutoraService {

	@Autowired
	private ConstrutoraRepository repository;
	@Autowired
	private ObraRepository obraRepository;
	private Integer duplicatedConstrutoraID;
	public Page<Construtora> readConstrutoras(Pageable pageable) {
		return repository.findAll(Pageable.unpaged());
	}

	public Optional<Construtora> readConstrutora(Integer id) {
		return repository.findById(id);
	}

	@Transactional
	public Construtora createConstrutora(Construtora construtora) {
		return repository.save(construtora);
	}

	@Transactional
	public Optional<Construtora> updateConstrutora(Integer id, ConstrutoraUpdateDto updatedConstrutora) {
		Construtora existingConstrutora = repository.getReferenceById(id);
		existingConstrutora.update(updatedConstrutora);
		repository.save(existingConstrutora);
		return Optional.ofNullable(existingConstrutora);
	}

	public Boolean verifyNameDuplication(Integer id, String nome)
	{
		List<Construtora> construtoras = repository.findAll();
		for (Construtora construtora : construtoras) {
			String nomeExistente = normalizeString(construtora.getNome());
			if (nomeExistente.equals(normalizeString(nome)) && !Objects.equals(construtora.getId(), id)) {
				duplicatedConstrutoraID = construtora.getId();
				return true;
			}
		}
		return false;
	}

	@Transactional
	public Optional<Construtora> mergeConstrutoras(Integer id)
	{
		List<Obra> obras = obraRepository.findObrasWhereConstrutoraId(id);
		Construtora existingConstrutora = repository.getReferenceById(duplicatedConstrutoraID);
		if(obras != null)
		{
			for(Obra obra : obras){
				obra.setConstrutora(existingConstrutora);
			}
		}
		duplicatedConstrutoraID = null;
		this.repository.deleteById(id);
		return Optional.of(existingConstrutora);
	}

	@Transactional
	public void deleteConstrutora(Integer id) {
		this.repository.deleteById(id);
	}
}
