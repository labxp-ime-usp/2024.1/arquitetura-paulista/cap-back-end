package br.com.usp.mac0472.cartografiapaulistana.utils;

import java.util.Collection;

public class FormListsValidators {
    public static <T> boolean hasOnlyNullValues(Collection<T> collection) {
        for(T item: collection) {
            if (item != null) {
                if (item instanceof String && item.equals("null")) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }
}