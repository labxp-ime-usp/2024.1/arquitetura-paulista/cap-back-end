package br.com.usp.mac0472.cartografiapaulistana.service;

import java.util.*;

import br.com.usp.mac0472.cartografiapaulistana.dto.escritorio.EscritorioUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Escritorio;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.repository.EscritorioRepository;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static br.com.usp.mac0472.cartografiapaulistana.utils.NameDuplicationUtil.normalizeString;

@Service
public class EscritorioService {

    @Autowired
    private EscritorioRepository repository;
    @Autowired
    private ObraRepository obraRepository;
    private Integer duplicatedEscritorioID;
    public Page<Escritorio> readEscritorios(Pageable pageable) {
        return repository.findAll(Pageable.unpaged());
    }
    public Optional<Escritorio> readEscritorio(Integer id) {
        return repository.findById(id);
    }
    public Escritorio createEscritorio(Escritorio escritorio) {
        return repository.save(escritorio);
    }

    @Transactional
    public Optional<Escritorio> updateEscritorio(Integer id, EscritorioUpdateDto updatedEscritorio)
    {
        Escritorio existingEscritorio = repository.getReferenceById(id);
        existingEscritorio.update(updatedEscritorio);
        repository.save(existingEscritorio);
        return Optional.ofNullable(existingEscritorio);
    }

    public Boolean verifyNameDuplication(Integer id, String nome)
    {
        List<Escritorio> escritorios = repository.findAll();
        for (Escritorio escritorio : escritorios) {
            String nomeExistente = normalizeString(escritorio.getNome());
            if (nomeExistente.equals(normalizeString(nome)) && !Objects.equals(escritorio.getId(), id)) {
                duplicatedEscritorioID = escritorio.getId();
                return true;
            }
        }
        return false;
    }

    @Transactional
    public Optional<Escritorio> mergeEscritorios(Integer id)
    {
        List<Obra> obras = obraRepository.findObrasWhereEscritorioId(id);
        Escritorio editedEscritorio = repository.getReferenceById(id);
        Escritorio existingEscritorio = repository.getReferenceById(duplicatedEscritorioID);
        if(obras != null)
        {
            for(Obra obra : obras){
                Set<Escritorio> escritoriosObra = new HashSet<>(obra.getEscritorios().stream().toList());
                escritoriosObra.add(existingEscritorio);
                escritoriosObra.remove(editedEscritorio);
                obra.getEscritorios().clear();
                obra.getEscritorios().addAll(escritoriosObra);
            }
        }
        duplicatedEscritorioID = null;
        this.repository.deleteById(id);
        return Optional.of(existingEscritorio);
    }

    @Transactional
    public void deleteEscritorio(Integer id) {
        this.repository.deleteById(id);
    }
}
