package br.com.usp.mac0472.cartografiapaulistana.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.usp.mac0472.cartografiapaulistana.model.Obra;

import java.util.List;

@Repository
public interface ObraRepository extends JpaRepository<Obra, Integer> {

	@Query("SELECT o FROM Obra o WHERE o.validadoProfessora = :validadasProfessora AND o.validadoDPH = :validadasDph")
	Page<Obra> findObrasWithValidation(Pageable pageable, Boolean validadasProfessora, Boolean validadasDph);

	@Query("SELECT o FROM Obra o")
	Page<Obra> findObras(Pageable pageable);

	@Query("SELECT o FROM Obra o JOIN o.escritorios e WHERE e.id = :escritorioId")
	List<Obra> findObrasWhereEscritorioId(Integer escritorioId);

	@Query("SELECT o FROM Obra o JOIN o.arquitetos e WHERE e.id = :arquitetoId")
	List<Obra> findObrasWhereArqId(Integer arquitetoId);

	@Query("SELECT o FROM Obra o JOIN o.construtora e WHERE e.id = :construtoraId")
	List<Obra> findObrasWhereConstrutoraId(Integer construtoraId);

	@Query("SELECT o FROM Obra o JOIN o.arquitetoReforma e WHERE e.id = :arquitetoId")
	List<Obra> findObrasWhereArqReformaId(Integer arquitetoId);

	@Query("SELECT DISTINCT o FROM Obra o LEFT JOIN FETCH o.arquitetos a WHERE " +
		   "LOWER(o.nomeOficial) LIKE LOWER(CONCAT('%',:pesquisa,'%')) " +
		   "OR LOWER(o.endereco.logradouro) LIKE LOWER(CONCAT('%',:pesquisa,'%')) " +
		   "OR LOWER(CONCAT(a.nome, ' ', COALESCE(a.nomeMeio, ''), ' ', a.sobrenome)) LIKE LOWER(CONCAT('%',:pesquisa,'%')) " +
		   "OR EXISTS (SELECT 1 FROM o.escritorios e WHERE e.nome LIKE %:pesquisa%) " +
		   "OR EXISTS (SELECT 1 FROM o.construtora e WHERE e.nome LIKE %:pesquisa%) " +
		   "OR EXISTS (SELECT 1 FROM o.arquitetoReforma e WHERE e.nome Like %:pesquisa%)"
	)
	Page<Obra> findObrasByNomeOficialArquitetoEndereco(Pageable pageable, String pesquisa);

	@Query("SELECT DISTINCT o FROM Obra o LEFT JOIN FETCH o.arquitetos a WHERE " +
		   "(o.validadoProfessora = :validadasProfessora AND o.validadoDPH = :validadasDph) " +
		   "AND (LOWER(o.nomeOficial) LIKE LOWER(CONCAT('%',:pesquisa,'%')) " +
		   "OR LOWER(o.endereco.logradouro) LIKE LOWER(CONCAT('%',:pesquisa,'%')) " +
		   "OR LOWER(CONCAT(a.nome, ' ', COALESCE(a.nomeMeio, ''), ' ', a.sobrenome)) LIKE LOWER(CONCAT('%',:pesquisa,'%')) " +
		   "OR EXISTS (SELECT 1 FROM o.escritorios e WHERE e.nome LIKE %:pesquisa%))"
	)
	Page<Obra> findValidationObrasByNomeOficialArquitetoEndereco(Pageable pageable, Boolean validadasProfessora, Boolean validadasDph, String pesquisa);

}

