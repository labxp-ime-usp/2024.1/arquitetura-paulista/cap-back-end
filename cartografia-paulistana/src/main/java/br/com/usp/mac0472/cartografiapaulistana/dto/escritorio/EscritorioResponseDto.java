package br.com.usp.mac0472.cartografiapaulistana.dto.escritorio;

import lombok.Data;
@Data
public class EscritorioResponseDto {
    Integer id;
    String nome;
}
