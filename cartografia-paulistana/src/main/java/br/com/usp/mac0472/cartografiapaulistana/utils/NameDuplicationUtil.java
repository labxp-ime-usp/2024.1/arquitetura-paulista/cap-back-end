package br.com.usp.mac0472.cartografiapaulistana.utils;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class NameDuplicationUtil {
    public static String normalizeString(String string)
    {
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(Normalizer.normalize(string.toLowerCase(), Normalizer.Form.NFD)).replaceAll("");
    }
}
