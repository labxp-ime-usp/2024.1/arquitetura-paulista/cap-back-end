package br.com.usp.mac0472.cartografiapaulistana.service;

import java.util.*;
import java.util.stream.Collectors;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoCreateDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import br.com.usp.mac0472.cartografiapaulistana.repository.ReferenciaRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;

import static br.com.usp.mac0472.cartografiapaulistana.utils.FormListsValidators.hasOnlyNullValues;

@Service
public class ObraService {

	@Autowired
	private ObraRepository repository;
	
	@Autowired
	private ConstrutoraService construtoraService;
	
	@Autowired
	private ArquitetoService arquitetoService;

	@Autowired
	private EscritorioService escritorioService;

	@Autowired
	private ReferenciaRepository referenciaRepository;

	public Page<Obra> readObras(Pageable pageable) {
		return repository.findObras(pageable);
	}

	public Page<Obra> readObrasWithValidation(Pageable pageable, Boolean validadasProfessora, Boolean validadasDph) {
		return repository.findObrasWithValidation(pageable, validadasProfessora, validadasDph);
	}

	public Optional<Obra> readObra(Integer id) {
		return repository.findById(id);
	}

	public Page<Obra> findObrasByNomeOficialArquitetoEndereco(Pageable pageable, String pesquisa){
		return repository.findObrasByNomeOficialArquitetoEndereco(pageable, pesquisa);
	}

	public Page<Obra> findValidationObrasByNomeOficialArquitetoEndereco(Pageable pageable, Boolean validadasProfessora, Boolean validadasDph, String pesquisa){
		return repository.findValidationObrasByNomeOficialArquitetoEndereco(pageable,validadasProfessora,validadasDph,pesquisa);
	}

	@Transactional
	public Obra createObra(Obra obra, List<String> arquitetosId, Integer construtoraId, Integer arquitetoReformaId, List<String> escritoriosId, Endereco endereco, List<String> referenciasUrls) {
		if(construtoraId != null)
		{
			Construtora construtora = construtoraService.readConstrutora(construtoraId)
					.orElseThrow(() -> new EntityNotFoundException("Construtora não encontrada."));
			obra.setConstrutora(construtora);
		}
		else {
			obra.setConstrutora(null);
		}

		if (!arquitetosId.isEmpty() && !hasOnlyNullValues(arquitetosId)) {
			Set<Arquiteto> arquitetos = arquitetosId.stream()
					.filter(Objects::nonNull) // Filter out null values
					.map(autoriaId -> {
						return arquitetoService.readArquiteto(Integer.parseInt(autoriaId))
								.orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
					})
					.collect(Collectors.toSet());

			obra.setArquitetos(arquitetos);
		}

		if(arquitetoReformaId != null)
		{
			Arquiteto arquitetoReforma = arquitetoService.readArquiteto(arquitetoReformaId)
					.orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
			obra.setArquitetoReforma(arquitetoReforma);
		}

        if (!escritoriosId.isEmpty() && !hasOnlyNullValues(escritoriosId)) {
			Set<Escritorio> escritorios = escritoriosId.stream()
					.filter(Objects::nonNull) // Filter out null values
					.map(escritorioId -> {
						return escritorioService.readEscritorio(Integer.parseInt(escritorioId))
								.orElseThrow(() -> new EntityNotFoundException("Escritório não encontrado."));
					})
					.collect(Collectors.toSet());

            obra.setEscritorios(escritorios);
        }

		obra.setValidadoDPH(false);
		obra.setValidadoProfessora(false);
		obra.setEndereco(endereco);
		Obra obraSalva = repository.save(obra);

		if (!referenciasUrls.isEmpty() && !hasOnlyNullValues(referenciasUrls)) {
			List<Referencia> referencias = referenciasUrls.stream().map(referenciaUrl -> new Referencia(referenciaUrl)).toList();
			referencias.forEach(referencia -> referencia.setObra(obraSalva));
			referenciaRepository.saveAll(referencias);
		}

		return obraSalva;
	}

	@Transactional
	public Optional<Obra> updateObra(Integer id, ObraUpdateDto updatedObra, Endereco endereco) {
		Obra existingObra = repository.getReferenceById(id);
		    List<Referencia> existingReferencias = existingObra.getReferencias() != null ? existingObra.getReferencias().stream().toList() : new ArrayList<>();
		if(updatedObra.construtora() != null)
		{
			Construtora construtora = construtoraService.readConstrutora(updatedObra.construtora())
					.orElseThrow(() -> new EntityNotFoundException("Construtora não encontrada."));
			existingObra.setConstrutora(construtora);
		}
		else {
			existingObra.setConstrutora(null);
		}
		if(updatedObra.arquitetoReforma() != null)
		{
			Arquiteto arquitetoReforma = arquitetoService.readArquiteto(updatedObra.arquitetoReforma())
					.orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
			existingObra.setArquitetoReforma(arquitetoReforma);
		}
		if (!updatedObra.escritorio().isEmpty() && !hasOnlyNullValues(updatedObra.escritorio())) {
			Set<Escritorio> escritorios = updatedObra.escritorio().stream()
					.filter(Objects::nonNull) // Filter out null values
					.map(escritorioId -> {
						return escritorioService.readEscritorio(Integer.parseInt(escritorioId))
								.orElseThrow(() -> new EntityNotFoundException("Escritório não encontrado."));
					})
					.collect(Collectors.toSet());
			existingObra.getEscritorios().clear();
			existingObra.getEscritorios().addAll(escritorios);
		} else {
			existingObra.getEscritorios().clear();
		}
		if (!updatedObra.autoria().isEmpty() && !hasOnlyNullValues(updatedObra.autoria())) {
			Set<Arquiteto> arquitetos = updatedObra.autoria().stream()
					.filter(Objects::nonNull) // Filter out null values
					.map(autoriaId -> {
						return arquitetoService.readArquiteto(Integer.parseInt(autoriaId))
								.orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
					})
					.collect(Collectors.toSet());
			existingObra.getArquitetos().clear();
			existingObra.getArquitetos().addAll(arquitetos);
		} else {
			existingObra.getArquitetos().clear();
		}
        List<Referencia> referencias = updatedObra.referencias().stream().map(referenciaUrl -> new Referencia(referenciaUrl)).toList();
		existingObra.setEndereco(endereco);
		existingObra.update(updatedObra);
		repository.save(existingObra);
		if (!existingReferencias.isEmpty() && !hasOnlyNullValues(existingReferencias) &&
				!referencias.isEmpty() && !hasOnlyNullValues(referencias)) {
			existingReferencias.forEach(referencia -> referenciaRepository.delete(referencia));
			referencias.forEach(referencia -> referencia.setObra(existingObra));
			referenciaRepository.saveAll(referencias);
		}

		return Optional.of(existingObra);
	}

	@Transactional
	public void deleteObra(Integer id) {
		repository.deleteById(id);
	}

	@Transactional
	public Obra validacaoProfessora(Integer id) {
		Obra existingObra = repository.findById(id).orElseThrow(() -> new EntityNotFoundException());
		existingObra.setValidadoProfessora(!existingObra.getValidadoProfessora());
		repository.save(existingObra);
		return existingObra;
	}

	@Transactional
	public Obra validacaoDPH(Integer id) {
		Obra existingObra = repository.findById(id).orElseThrow(() -> new EntityNotFoundException());
		existingObra.setValidadoDPH(!existingObra.getValidadoDPH());
		repository.save(existingObra);
		return existingObra;
	}
}
