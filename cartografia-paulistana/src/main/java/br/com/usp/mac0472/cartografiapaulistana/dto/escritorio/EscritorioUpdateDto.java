package br.com.usp.mac0472.cartografiapaulistana.dto.escritorio;

public record EscritorioUpdateDto(String nome) {
}
