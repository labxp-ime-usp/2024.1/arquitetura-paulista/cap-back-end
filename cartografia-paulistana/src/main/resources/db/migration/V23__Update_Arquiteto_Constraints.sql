ALTER TABLE arquiteto ADD CONSTRAINT unique_nome_completo UNIQUE (nome, nome_meio, sobrenome);
ALTER TABLE arquiteto DROP CONSTRAINT unique_nome_sobrenome;