CREATE TABLE IF NOT EXISTS escritorio(
    id INT AUTO_INCREMENT,
    nome VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE escritorio ADD CONSTRAINT unique_nome_escritorio UNIQUE (nome);