CREATE TABLE IF NOT EXISTS escritorio_obra(
    id INT AUTO_INCREMENT,
    escritorio_id INT,
    obra_id INT,
    FOREIGN KEY (escritorio_id) REFERENCES escritorio(id),
    FOREIGN KEY (obra_id) REFERENCES obra(id),
    PRIMARY KEY(id)
);