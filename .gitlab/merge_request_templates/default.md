### Descrição
TO DO: descrever o que esse merge request está resolvendo/modificando/inserindo no projeto.

### Mudanças feitas
TO DO: inserir detalhes sobre o que está sendo modificado/inserido. Se for o caso, insira screenshots e trechos de código importantes.

### Issues relacionadas
TO DO: inserir links de issues que estão relacionadas às issues ou features.
!! Remova esta seção quando for o caso.

### Informações adicionais
TO DO: insira quaisquer informações adicionais ou outras considerações para os revisores do merge request.
!! Remova esta seção quando for o caso.

### Checklists
- [ ] O código inclui testes automatizados
- [ ] Foram feitas alterações necessárias nas documentações existentes
- [ ] Existem issues ou features requests mapeadas à mudança